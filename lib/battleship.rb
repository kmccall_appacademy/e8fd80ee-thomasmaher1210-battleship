# A BattleshipGame class to enforce rules and run the game. The game should
#  keep a reference to the Player, as well as the Board.

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play
    # runs the game by calling play_turn until the game is over.
    until game_over
      play_turn
    end
    puts "You win!"
  end


  def play_turn
    # gets a guess from the player and makes an attack.
    pos = player.get_play
    attack(pos)
  end

  def attack(pos)
    # Marks the board at pos, destroying or replacing any ship that
    # might be there.
    # p board.grid, pos, board.grid[pos.first][pos.last],
      board.grid[pos.first][pos.last] = :x
  end

  def display_status
    # Prints information on the current state of the game, including board state and the number of ships remaining.
    print board.grid
  end


end
