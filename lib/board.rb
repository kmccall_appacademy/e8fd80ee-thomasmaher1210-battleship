# A Board class with an underlying grid (a two-dimensional Array),
# where each element in a row represents a ship, open water, or a space
# that has already been attacked. I used the symbol :s to represent an
#  undamaged ship (or ship segment), nil for empty space, and :x to
#  represent a destroyed space. Useful Board methods:



class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end


  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    # returns the number of valid targets (ships) remaining
    count = 0
    grid.each do |row|
      row.each {|col| count += 1 if col == :s}
    end
    count
  end


  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(position, value)
      row, col = pos
      grid[row][col] = value
  end

  def empty?(pos= [nil, nil] )
    if pos.first != nil
      return grid[pos.first][pos.last] == nil
    end
    if count == 0
      return true
    end
    false
  end

  def full?
    num_ships = grid.flatten.count(:s)
    num_ships == 4
  end

  def place_random_ship
    #Board#populate_grid to randomly distribute ships across the board
    if full?
      raise "board is full"
    elsif grid.flatten.count(:s) < 4
        grid[rand(2)][rand(2)] = :s
    end
  end

  def won?
    grid.flatten.count(:s) == 0
  end

  def in_range?(pos)
  end

end

# grid.each do |pos|
#   b = rand(2)
#   if pos == nil && b == 0
#     pos = :s : next
#   end
